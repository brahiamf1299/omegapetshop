const clienteModelo =require ("../modelos/clienteModelo");
const clienteOperaciones ={}

clienteOperaciones.crearCliente = async (req,res)=>{
    try{
        const objeto = req.body;
        console.log(objeto);
        const cliente = new clienteModelo(objeto);
        const clienteguardado = await cliente.save();
        res.status(201).send(clienteguardado);
    }catch(error){
        res.status(400).send("Mala petición: "+error);
    }
}

clienteOperaciones.buscarCliente = async (req, res)=>{
    try {
        const id = req.params.id;
        const cliente = await clienteModelo.findById(id);
        if (cliente != null){
            res.status(200).send(cliente);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

clienteOperaciones.modificarCliente = async (req, res)=>{
    try {
        const id = req.params.id;
        const body = req.body;
        const datosActualizar = {
            nombres: body.nombres,
            direccion: body.direccion,
            // identificacion: body.identificacion,
            contraseña: body.contraseña,
            correo: body.correo,
            telefono: body.telefono,
           
        }
        const clienteActualizado = await clienteModelo.findByIdAndUpdate(id, datosActualizar, { new : true });
        if (clienteActualizado != null) {
            res.status(200).send(clienteActualizado);
        }
        else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. "+error);
    }
}

module.exports = clienteOperaciones;