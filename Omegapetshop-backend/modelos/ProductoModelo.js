const mongoose = require("mongoose");

const productoSchema = mongoose.Schema({
    codigo: {type: Number, required: true, unique: true},
    nombre: {type: String, maxLength: 40, required: true, unique: false },
    marca: {type: String, maxLength: 40, required: true, unique: false},
    precio: {type: Number, required: true, unique: false},
    peso: {type: Number, required: false, unique: false},
    categoria: {type: String, maxLength: 40, required: true, unique:false},
    cantidad: {type: Number, required: true, unique:false}
});

module.exports= mongoose.model("productos",productoSchema);